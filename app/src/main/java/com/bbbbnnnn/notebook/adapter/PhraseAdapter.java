package com.bbbbnnnn.notebook.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bbbbnnnn.notebook.R;
import com.bbbbnnnn.notebook.model.category.Phrase;

import java.util.ArrayList;

public class PhraseAdapter extends RecyclerView.Adapter<PhraseAdapter.PhraseHolder> {

    private ArrayList<Phrase> data;
    private LayoutInflater inflater;
    private PhraseItemListener listener;

    public PhraseAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setData(ArrayList<Phrase> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void setListener(PhraseItemListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public PhraseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_phrase, parent, false);
        return new PhraseHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PhraseHolder holder,final int position) {
        Phrase item = data.get(position);
        holder.bindData(item);
        if (listener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPhraseItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class PhraseHolder extends RecyclerView.ViewHolder {
        private TextView tvPhrase;
        private ImageView imPhrase;

        public PhraseHolder(@NonNull View itemView) {
            super(itemView);
            tvPhrase = itemView.findViewById(R.id.tv_phrase);
            imPhrase = itemView.findViewById(R.id.im_phrase);
        }

        public void bindData(Phrase item) {
            tvPhrase.setText(item.getName());
        }
    }

    public interface PhraseItemListener {
        void onPhraseItemClick(int position);
    }
}
