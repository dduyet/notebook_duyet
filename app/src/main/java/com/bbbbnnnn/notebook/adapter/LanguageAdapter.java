package com.bbbbnnnn.notebook.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bbbbnnnn.notebook.R;
import com.bbbbnnnn.notebook.model.language.Language;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.LanguageHolder> {

    private ArrayList<Language> data;
    private LayoutInflater inflater;
    private LanguageItemListener listener;

    public LanguageAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setData(ArrayList<Language> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void setListener(LanguageItemListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public LanguageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_language, parent, false);
        return new LanguageHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull LanguageHolder holder, final int position) {
        Language item = data.get(position);
        holder.bindData(item);
        if (listener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onLangItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class LanguageHolder extends RecyclerView.ViewHolder {
        private ImageView imLang;
        private TextView tvLang;

        public LanguageHolder(@NonNull View itemView) {
            super(itemView);
            imLang = itemView.findViewById(R.id.im_language);
            tvLang = itemView.findViewById(R.id.tv_language);
        }

        public void bindData(Language item) {
            tvLang.setText(item.getTranslate());
            loadImage(imLang, item.getImage());
        }
    }
    private void loadImage(ImageView im, String link) {
        Glide.with(im)
                .load(link)
                .error(R.mipmap.ic_launcher)
                .into(im);
    }

    public interface LanguageItemListener {
        void onLangItemClick(int position);
    }
}
