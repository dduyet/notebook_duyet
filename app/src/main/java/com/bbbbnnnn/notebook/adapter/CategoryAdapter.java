package com.bbbbnnnn.notebook.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bbbbnnnn.notebook.R;
import com.bbbbnnnn.notebook.model.category.Category;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryHolder> {

    private ArrayList<Category> data;
    private LayoutInflater inflater;
    private CategoryItemListener listener;

    public CategoryAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setData(ArrayList<Category> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void setListener(CategoryItemListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public CategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_category, parent, false);
        return new CategoryHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryHolder holder, int position) {
        Category item = data.get(position);
        holder.bindData(item);
        if (listener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCateItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class CategoryHolder extends RecyclerView.ViewHolder {

        private ImageView imCate;
        private TextView tvCate;

        public CategoryHolder(@NonNull View itemView) {
            super(itemView);

            imCate = itemView.findViewById(R.id.im_cate);
            tvCate = itemView.findViewById(R.id.tv_cate);
        }

        public void bindData(Category item) {
            loadImage(imCate, item.getImage());
            tvCate.setText(item.getName());
        }
    }

    private void loadImage(ImageView im, String link) {
        Glide.with(im)
                .load(link)
                .error(R.mipmap.ic_launcher)
                .into(im);
    }

    public interface CategoryItemListener {
        void onCateItemClick(int position);
    }
}
