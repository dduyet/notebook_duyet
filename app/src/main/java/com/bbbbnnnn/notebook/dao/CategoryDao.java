package com.bbbbnnnn.notebook.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.bbbbnnnn.notebook.model.category.Category;
import com.bbbbnnnn.notebook.model.category.Phrase;

import java.util.List;

@Dao
public interface CategoryDao {
    @Query("SELECT * FROM Category")
    List<Category> getCategory();

    @Insert
    void insert(Category... cate);

    @Update
    void update(Category... cate);

    @Delete
    void delete(Category... cate);
}
