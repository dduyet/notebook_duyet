package com.bbbbnnnn.notebook.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.bbbbnnnn.notebook.model.language.Language;

import java.util.List;

@Dao
public interface LanguageDao {
    @Query("SELECT * FROM Language")
    List<Language> getLanguage();

    @Insert
    void insert(Language... lang);

    @Update
    void update(Language... lang);

    @Delete
    void delete(Language... lang);
}
