package com.bbbbnnnn.notebook.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.bbbbnnnn.notebook.model.category.Phrase;

import java.util.List;

@Dao
public interface PhraseDao {
    @Query("SELECT * FROM Phrase")
    List<Phrase> getPhrase();

    @Insert
    void insert(Phrase... phrases);

    @Update
    void update(Phrase... phrases);

    @Delete
    void delete(Phrase... phrases);
}
