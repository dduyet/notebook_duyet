package com.bbbbnnnn.notebook.api;

import com.bbbbnnnn.notebook.model.category.CategoryResponse;
import com.bbbbnnnn.notebook.model.category.PhraseResponse;
import com.bbbbnnnn.notebook.model.language.LanguageResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {
    @GET("get-all-language")
    Call<LanguageResponse> addLanguage();

    @GET("get-all-category")
    Call<CategoryResponse> addCategory();

    @GET("get-all-category")
    Call<PhraseResponse> addPhrase();
}
