package com.bbbbnnnn.notebook.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class ApiBuilder {
    private static Api api;

    public static Api getInstance() {
        if (api == null) {
            api = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://notebook.mesaenglish.com/api/v1/")
                    .build()
                    .create(Api.class);
        }
        return api;
    }

}
