package com.bbbbnnnn.notebook.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;

import com.bbbbnnnn.notebook.R;
import com.bbbbnnnn.notebook.adapter.LanguageAdapter;
import com.bbbbnnnn.notebook.api.ApiBuilder;
import com.bbbbnnnn.notebook.dao.AppDatabase;
import com.bbbbnnnn.notebook.model.language.Language;
import com.bbbbnnnn.notebook.model.language.LanguageResponse;
import com.bbbbnnnn.notebook.utils.PermissionUtils;
import com.bbbbnnnn.notebook.utils.ToastUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LanguageActivity extends AppCompatActivity implements LanguageAdapter.LanguageItemListener, Callback<LanguageResponse> {

    private static final int REQUEST_PERMISSIONS = 1;
    private RecyclerView lvLang;
    private ArrayList<Language> data;
    private LanguageAdapter adapter;
    private final String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);

        if (PermissionUtils.checkPermissions(this, PERMISSIONS)) {
            initViews();
            initData();
        } else
            PermissionUtils.requestPermissions(this, PERMISSIONS, REQUEST_PERMISSIONS);

    }

    private void initData() {
        data = new ArrayList<>();
        adapter.setData(data);

        ApiBuilder.getInstance().addLanguage().enqueue(this);
    }

    private void initViews() {
        lvLang = findViewById(R.id.lv_languages);
        adapter = new LanguageAdapter(getLayoutInflater());
        lvLang.setAdapter(adapter);
        adapter.setListener(this);
    }


    @Override
    public void onResponse(Call<LanguageResponse> call, Response<LanguageResponse> response) {
        LanguageResponse languageResponse = response.body();
        data = languageResponse.getData();
        adapter.setData(data);

    }

    @Override
    public void onFailure(Call<LanguageResponse> call, Throwable t) {
        ToastUtils.show(this, "fail");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        boolean result = PermissionUtils.checkPermissions(this, permissions);
        if (result) {
            initViews();
            initData();
        } else
            finish();
    }

    @Override
    public void onLangItemClick(int position) {
        switch (position) {
            case 0:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            case 1:
                ToastUtils.show(this, "Tiếng Thái");
                break;
            case 2:
                ToastUtils.show(this, "Tiếng Nhật");
                break;
            case 3:
                ToastUtils.show(this, "Tiếng Hàn");
                break;
        }
    }
}
