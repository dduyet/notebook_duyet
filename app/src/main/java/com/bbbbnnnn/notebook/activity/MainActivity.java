package com.bbbbnnnn.notebook.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;

import com.bbbbnnnn.notebook.R;
import com.bbbbnnnn.notebook.base.BaseFragment;
import com.bbbbnnnn.notebook.fragment.CategoryFragment;
import com.bbbbnnnn.notebook.fragment.FavoriteFragment;
import com.bbbbnnnn.notebook.fragment.GrammarFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private CategoryFragment fmCategory = new CategoryFragment();
    private GrammarFragment fmGrammar = new GrammarFragment();
    private FavoriteFragment fmFavorite = new FavoriteFragment();

    private BottomNavigationView nav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initFragment();
        nav = findViewById(R.id.nav);
        nav.setOnNavigationItemSelectedListener(this);
    }
    private void initFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.container, fmCategory);
        transaction.add(R.id.container, fmGrammar);
        transaction.add(R.id.container, fmFavorite);
        transaction.commit();
        showFragment(fmCategory);
    }
    private void showFragment(BaseFragment fmShow) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        transaction.hide(fmCategory);
        transaction.hide(fmGrammar);
        transaction.hide(fmFavorite);

        transaction.show(fmShow);
        transaction.commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_category:
                showFragment(fmCategory);
                break;
            case R.id.nav_grammar:
                showFragment(fmGrammar);
                break;
            case R.id.nav_favorite:
                showFragment(fmFavorite);
                break;
        }
        return true;
    }
}
