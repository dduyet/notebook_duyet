package com.bbbbnnnn.notebook.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.bbbbnnnn.notebook.R;
import com.bbbbnnnn.notebook.adapter.PhraseAdapter;
import com.bbbbnnnn.notebook.api.ApiBuilder;
import com.bbbbnnnn.notebook.fragment.CategoryFragment;
import com.bbbbnnnn.notebook.model.category.Category;
import com.bbbbnnnn.notebook.model.category.Phrase;
import com.bbbbnnnn.notebook.model.category.PhraseResponse;
import com.bbbbnnnn.notebook.utils.ToastUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PhraseActivity extends AppCompatActivity implements PhraseAdapter.PhraseItemListener, Callback<PhraseResponse> {

    private RecyclerView lvPhrase;
    private ArrayList<Phrase> data;
    private PhraseAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phrase);

        initViews();
        initData();
    }

    private void initData() {
        Intent intent = getIntent();
        int position = intent.getIntExtra(CategoryFragment.EXTRA_POSITION,-1);
//        data.get(position).getCategoryId();

        data = new ArrayList<>();
        adapter.setData(data);

        ApiBuilder.getInstance().addPhrase().enqueue(this);
    }

    private void initViews() {
        lvPhrase = findViewById(R.id.lv_phrase);
        adapter = new PhraseAdapter(getLayoutInflater());
        lvPhrase.setAdapter(adapter);
        adapter.setListener(this);

    }

    @Override
    public void onPhraseItemClick(int position) {

    }

    @Override
    public void onResponse(Call<PhraseResponse> call, Response<PhraseResponse> response) {
        PhraseResponse phraseResponse = response.body();
        data = phraseResponse.getPhrases();
        adapter.setData(data);
    }

    @Override
    public void onFailure(Call<PhraseResponse> call, Throwable t) {
        ToastUtils.show(this,"fail");
    }
}
