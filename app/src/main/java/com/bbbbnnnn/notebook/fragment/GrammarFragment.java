package com.bbbbnnnn.notebook.fragment;

import com.bbbbnnnn.notebook.R;
import com.bbbbnnnn.notebook.base.BaseFragment;

public class GrammarFragment extends BaseFragment {
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_grammar;
    }
}
