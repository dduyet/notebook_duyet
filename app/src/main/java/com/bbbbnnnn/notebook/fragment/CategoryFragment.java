package com.bbbbnnnn.notebook.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.bbbbnnnn.notebook.R;
import com.bbbbnnnn.notebook.activity.PhraseActivity;
import com.bbbbnnnn.notebook.adapter.CategoryAdapter;
import com.bbbbnnnn.notebook.api.ApiBuilder;
import com.bbbbnnnn.notebook.base.BaseFragment;
import com.bbbbnnnn.notebook.model.category.Category;
import com.bbbbnnnn.notebook.model.category.CategoryResponse;
import com.bbbbnnnn.notebook.utils.ToastUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryFragment extends BaseFragment implements CategoryAdapter.CategoryItemListener, Callback<CategoryResponse> {

    public static final String EXTRA_POSITION = "extra.position";
    private RecyclerView lvCate;
    private ArrayList<Category> data;
    private CategoryAdapter adapter;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_category;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
        initData();
    }

    private void initData() {
        data = new ArrayList<>();
        adapter.setData(data);

        ApiBuilder.getInstance().addCategory()
                .enqueue(this);
    }

    private void initViews() {
        lvCate = findViewById(R.id.lv_cate);
        adapter = new CategoryAdapter(getLayoutInflater());
        lvCate.setAdapter(adapter);
        adapter.setListener(this);

        StaggeredGridLayoutManager staggeredGridLayoutManager =
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        lvCate.setLayoutManager(staggeredGridLayoutManager);
    }

    @Override
    public void onCateItemClick(int position) {
        ToastUtils.show(getContext(), data.get(position).getName());
        Intent intent = new Intent(getActivity(), PhraseActivity.class);
        intent.putExtra(EXTRA_POSITION, position);
        startActivity(intent);
//        switch (position) {
//            case 0:
//                break;
//            case 1:
//                break;
//            case 2:
//                break;
//            case 3:
//                break;
//            case 4:
//                break;
//            case 5:
//                break;
//            case 6:
//                break;
//            case 7:
//                break;
//        }

    }

    @Override
    public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
        CategoryResponse categoryResponse = response.body();
        data = categoryResponse.getData();
        adapter.setData(data);
    }

    @Override
    public void onFailure(Call<CategoryResponse> call, Throwable t) {
        ToastUtils.show(getContext(), "fail");
    }
}
