package com.bbbbnnnn.notebook.model.language;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity
public class Language {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @SerializedName("id")
    private int id;

    @ColumnInfo
    @SerializedName("translate")
    private String translate;

    @ColumnInfo
    @SerializedName("image")
    private String image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTranslate() {
        return translate;
    }

    public void setTranslate(String translate) {
        this.translate = translate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
