package com.bbbbnnnn.notebook.model.language;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LanguageResponse {

    @SerializedName("data")
    private ArrayList<Language> data;

    public ArrayList<Language> getData() {
        return data;
    }
}
