package com.bbbbnnnn.notebook.model.category;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CategoryResponse {
    @SerializedName("data")
    private ArrayList<Category> data;

    public ArrayList<Category> getData() {
        return data;
    }
}
