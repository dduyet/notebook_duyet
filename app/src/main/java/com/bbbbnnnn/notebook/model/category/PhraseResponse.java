package com.bbbbnnnn.notebook.model.category;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PhraseResponse {
    @SerializedName("phrase")
    private ArrayList<Phrase> phrases;

    public ArrayList<Phrase> getPhrases() {
        return phrases;
    }
}
